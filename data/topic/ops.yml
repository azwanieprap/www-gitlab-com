description: GitLab is not just an ops tool, it's a tool for ops teams.
canonical_path: /topics/ops/
file_name: ops
twitter_image: /images/opengraph/iac-gitops.png
title: GitLab for Ops
header_body: >-
  GitLab is not just an ops tool, it's a tool for ops teams. Learn how your
  operations personnel can use GitLab for monitoring live applications, managing
  Kubernetes environments, and incident response.


  [Watch a GitLab demo →](/demo/) 
cover_image: null
body: >-
  ## GitLab for Ops teams


  GitLab has long been a favorite of Dev teams, but GitLab is for Ops too. A variety of operations personnel from Sys Admins and IT Ops Engineers to SREs and DevOps Engineers can benefit from GitLab's built-in Ops tools as part of a single application for the entire software development and operations lifecycle.


  [See observability capabilities →](/stages-devops-lifecycle/monitor/)




  ## Monitor Kubernetes Environments


  For cloud native applications, GitLab may be able to completely replace tools like Datadog, New Relic, and Splunk. With Prometheus, Sentry, and Jaeger integrations, GitLab comes with everything you need out of the box for your micro-services based applications. Using GitLab lowers costs by offloading your cloud native monitoring from legacy pay-per-use tools to GitLab's built-in capabilities.


  [Prometheus integration →](https://docs.gitlab.com/ee/user/project/integrations/prometheus.html#prometheus-integration)




  ## Use Auto-generated pipelines


  Get started quickly with GitLab's Auto DevOps capabilities. With a few clicks, Auto DevOps configures an entire CI/CD pipeline that detects your project's attributes and runs a pipeline based on best practices learned from more than 100K businesses using GitLab. Auto DevOps is fully configurable when you are ready to customize your pipelines to meet your exact specifications.


  [Auto DevOps →](/stages-devops-lifecycle/auto-devops/)




  ## View all pipelines at a glance


  Know which projects are green and which are red with a single view. The [Operations Dashboard](https://docs.gitlab.com/ee/user/operations_dashboard/) provides a summary of each project’s operational health, including pipeline and alert status so you can quickly diagnose system-wide problems or drill down into the specific commit causing a failure.


  [Operations Dashboard →](https://docs.gitlab.com/ee/user/operations_dashboard/)




  ## Incident Management


  Get up and running faster with a complete DevOps platform featuring incident management built-in. Your Agile project planning, source code, pipelines, and cloud native monitoring all live in GitLab. Imagine having all of this data at your fingertips seamlessly available from your incident management application.


  [Incident management →](/handbook/engineering/infrastructure/incident-management/)




  ## Serverless


  Run your own Functions-as-a-Service (FaaS) using GitLab Serverless. Functions and even container-based applications can be easily deployed that auto-scale up to meet demand then down to zero when there's no usage.


  [GitLab Serverless →](/topics/serverless/)
cta_banner: []
