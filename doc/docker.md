# Use Docker to render the website

If you don't have a proper environment available to run Middleman, you can use
Docker to provide one for you locally on your machine. To have this
working correctly, you need to have at least Git and Docker installed, and an
internet connection available.

> **Note**: This repository uses `git-lfs` extension for versioning large files. Before clone you need [to install](https://git-lfs.github.com/) it.

1. Clone the repository in a local folder
   
   ```sh
   git clone git@gitlab.com:gitlab-com/www-gitlab-com.git
   ```
>  **Note**: If your git clone is timing out, update the ServerAliveInterval in ~/.ssh/config to a larger number. For example, below is the content of the ~/.ssh/config file. <br> 
> ```
> Host *
>    ServerAliveInterval 1200
>    TCPKeepAlive yes 
>    IPQoS=throughput
>```

2. Create a Docker container - **NOTE: Change `sites/mysite` to either `sites/handbook` (works currently) or omit it for the rest of the site (which will soon move under `sites/uncategorized`)
   ```sh
   docker create --name middleman -v "$PWD/www-gitlab-com":/site -w /site -p 4567:4567 \
   -e LC_ALL=C.UTF-8 ruby:3.0 /bin/bash -c 'bundle install && cd sites/mysite && bundle exec middleman'
   ```

   > **Note**: `$PWD` prints the current working directory. If your docker container log
   > says `Could not locate Gemfile`, adjust the path string to match where you are running
   > `docker create`. For example, based on your current directory, your command may change
   > to something like this by removing `/www-gitlab-com` from the string:
   >
   > ```shell
   > user www-gitlab-com %** docker create --name middleman -v `"$PWD"`:/site -w /site -p 4567:4567 \
   > -e LC_ALL=C.UTF-8 ruby:3.0 /bin/bash -c 'bundle install && cd sites/mysite && bundle exec middleman'
   > ```


3. Start the container
   ```sh
   docker start middleman
   ```

4. Connect to http://localhost:4567

> **Note**: You won't be able to connect immediately. Middleman takes a few minutes to render the site. Run `docker ps -ls` to see if middleman is still running.

5. Change your original content as usual, and see the changes in the browser as soon as
you save the new version of the file (otherwise, just restart the container)

6. When you have finished, stop the container
   ```sh
   docker stop middleman
   ```

> **Note**: Subsequent runs will just require `docker start middleman`.
